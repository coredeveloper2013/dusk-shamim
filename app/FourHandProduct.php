<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FourHandProduct extends Model
{
    //
    protected $table = 'fourhandproducts';
}
