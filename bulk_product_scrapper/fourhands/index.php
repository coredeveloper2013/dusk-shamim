<?php
include '../simplehtmldom_1_5/simple_html_dom.php';

// database starts
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "dusk_scrapper";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error):
    die("Connection failed: " . $conn->connect_error);
endif;
// database ends

$html   = new simple_html_dom();
$counter = 0;
$product_links = [];

for( $i=1; $i<=25; $i++ ):

    $html->load( file_get_contents( $i . '.html') );
    // gain the product list
    foreach($html->find('a.product__img') as $element):
        $product_links[] = [
            'slug' => str_replace( array('product/'), '', trim( parse_url( trim( strip_tags( stripslashes($element->href) ) ), PHP_URL_PATH ), '/' ) ),
            'link' => trim( strip_tags( stripslashes($element->href) ) ),
        ];
    endforeach;

endfor;

    // print_r($product_links);

    // save the list to file
    // if( !empty($product_links) ):
    //
    //     $list = array($product_links);
    //     $fp = fopen('product_list.csv', 'w');
    //
    //     foreach ($product_links as $fields):
    //         fputcsv($fp, array($fields));
    //     endforeach;
    //
    //     fclose($fp);
    //
    // endif;

// save the list to database if not exists
if( !empty($product_links) ):

    foreach ($product_links as $fields):

        $sql = "SELECT * FROM fourhandslugs where slug='{$fields['slug']}'";
        $result = $conn->query($sql);
        if($result->num_rows > 0):
            // while( $row = $result->fetch_assoc() ):
            //     echo "id: >>>" . $row["productId"] . PHP_EOL;
            // endwhile;
            echo "Reject Record Exists >> " . $fields['slug'] . PHP_EOL;
            continue;
        endif;

        $sql = "INSERT INTO fourhandslugs (slug, links, status, created_at) VALUES ('{$fields['slug']}', '{$fields['link']}', '{false}', NOW())";
        if($conn->query($sql) === TRUE):
            echo "Record Added >> ".$fields['slug'];
            echo PHP_EOL;
        else:
            echo "Error >> " . $sql . "<br>" . $conn->error;
            echo PHP_EOL;
        endif;
        $counter++;

    endforeach;

    echo "Total Products >> " . $counter;

endif;

$conn->close();
