<?php
include '../simplehtmldom_1_5/simple_html_dom.php';

// database starts
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "dusk_scrapper";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error):
    die("Connection failed: " . $conn->connect_error);
endif;
// database ends

$html   = new simple_html_dom();

$html->load( file_get_contents('filtered_product.html') );

$counter = 0;
$product_links = [];

// gain the product list
foreach($html->find('a') as $element):

    if( strpos( $element->href, 'ItemCode' ) !== false ):

        parse_str(parse_url($element->href, PHP_URL_QUERY), $output);

        $product_links[] = [
            'productId' => trim( strip_tags( stripslashes($output['ItemCode']) ) ),
            'link'      => trim( strip_tags( stripslashes( str_replace(array('&amp;Category=All'), '', $element->href) ) ) ),
        ];

    else:
        continue;
    endif;

endforeach;

// print_r($product_links);

// save the list to file
// if( !empty($product_links) ):
//
//     $list = array($product_links);
//     $fp = fopen('product_list.csv', 'w');
//
//     foreach ($product_links as $fields):
//         fputcsv($fp, array($fields));
//     endforeach;
//
//     fclose($fp);
//
// endif;

// save the list to database if not exists
if( !empty($product_links) ):

    foreach ($product_links as $fields):

        $sql = "SELECT * FROM links where productId=".$fields["productId"];
        $result = $conn->query($sql);
        if($result->num_rows > 0):
            // while( $row = $result->fetch_assoc() ):
            //     echo "id: >>>" . $row["productId"] . PHP_EOL;
            // endwhile;
            echo "Reject Record Exists >> " . $fields["productId"] . PHP_EOL;
            continue;
        endif;

        $sql = "INSERT INTO links (productId, links, status, created_at) VALUES ('{$fields['productId']}', '{$fields['link']}', '{false}', NOW())";
        if($conn->query($sql) === TRUE):
            echo "Record Added >> ".$fields['productId'];
            echo PHP_EOL;
        else:
            echo "Error >> " . $sql . "<br>" . $conn->error;
            echo PHP_EOL;
        endif;

    endforeach;

endif;

$conn->close();
